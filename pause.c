/*******************************************************************************************
*
*   raylib exercise - pause and reset
*
*   This exercise has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013 Ramon Santamaria (Ray San)
*
********************************************************************************************/
 
#include "raylib.h"
 
int main()
{
    // Initialization
    //---------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
 
    InitWindow(screenWidth, screenHeight, "raylib exercise - pause and reset");
   
    int screenRightLimit = screenWidth - 150;
   
    int ballPosX = screenRightLimit / 2;
    int ballPosY = screenHeight / 2;
    int ballRadio = 20;
    int ballSpeedX = 4;
    int ballSpeedY = 4;
    Color ballColor = MAROON;
   
    bool pause = false;
    const char textPause[30] = "GAME PAUSED";
 
    SetTargetFPS(60);
    //----------------------------------------------------------
   
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //-----------------------------------------------------
        if (!pause)
        {
            ballPosX += ballSpeedX;
            ballPosY += ballSpeedY;
           
            if ((ballPosX >= (screenRightLimit - ballRadio)) || (ballPosX <= ballRadio))
            {
                ballSpeedX *= -1;
            }
           
            if ((ballPosY >= (screenHeight - ballRadio)) || (ballPosY <= ballRadio))
            {
                ballSpeedY *= -1;
            }
        }
       
        if (IsKeyPressed('P') || IsKeyPressed('p')) pause = !pause;
       
        if (IsKeyPressed('R') || IsKeyPressed('r'))
        {
            ballPosX = screenRightLimit / 2;
            ballPosY = screenHeight / 2;
        }
        //-----------------------------------------------------
       
        // Draw
        //-----------------------------------------------------
        BeginDrawing();
       
            ClearBackground(LIGHTGRAY);
           
            DrawRectangle(screenRightLimit, 0, screenWidth, screenHeight, RAYWHITE);
       
            DrawText("[P] PAUSE", screenRightLimit + 20, 20, 20, BLACK);
            DrawText("[R] RESET", screenRightLimit + 20, 50, 20, BLACK);
            DrawText("[ESC] EXIT", screenRightLimit + 20, 80, 20, BLACK);
 
            DrawCircle(ballPosX, ballPosY, ballRadio, ballColor);
       
            if (pause)
            {
                DrawText(textPause, screenRightLimit / 2 - MeasureText(textPause, 20) / 2, screenHeight / 2 - 10, 20, BLACK);            
            }  
 
            DrawFPS(10, 10);
       
        EndDrawing();
        //-----------------------------------------------------
    }
 
    // De-Initialization
    //---------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //----------------------------------------------------------
   
    return 0;
}