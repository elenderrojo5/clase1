#include <stdio.h>
 
/* function declaration */
int max(int num1, int num2);
int min(int num1, int num2);
int add(int num1, int num2);
int mult(int num1, int num2);
 
int main () {

   /* local variable definition */
   int a = 0;
   int b = 0;
   int ret;
   int ret2;
   int ret3;
   int ret4;
    
    printf("dame un numero\n");
    scanf("%i",&a);
    getchar();
    printf("dame otro numero\n");
    scanf("%i",&b);
    getchar();
    
   /* calling a function to get max value */
   ret = max(a, b);
    ret2 = min(a, b);
    ret3 = add(a, b);
    ret4 = mult(a, b);
    
   printf( "Max value is : %d\n", ret );
   printf( "Min value is : %d\n", ret2 );
   printf("%i + %i=%i\n", a, b, ret3);
   printf( "%i * %i=%i\n", a, b, ret4 );
    
    getchar();
   return 0;
}
 
/* function returning the max between two numbers */
int max(int num1, int num2) {

   /* local variable declaration */
   int result;
 
   if (num1 > num2)
      result = num1;
   else
      result = num2;
 
   return result; 
}
int min(int num1, int num2) {

   /* local variable declaration */
   int result;
 
   if (num1 < num2)
      result = num1;
   else
      result = num2;
 
   return result; 
}
int add(int num1, int num2) {

   /* local variable declaration */
   int result;
 
   result = num1 + num2;
   
   return result; 
}
int mult(int num1, int num2) {

   /* local variable declaration */
   int result;
 
   result = num1 * num2;
 
   return result; 
}