/*******************************************************************************************
*
*   raylib [core] example - basic window
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013-2016 Ramon Santamaria (@raysan5)
*
********************************************************************************************/
 
#include "raylib.h"
 
typedef enum direccion_t{
    DERECHA,
    ABAJO,
    IZQUIERDA,
    ARRIBA
}direccion;
 
#define VELOCIDAD 10
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 800;
    int x = 0, y = 0;
   
    direccion dir = DERECHA;
   
 
    InitWindow(screenWidth, screenHeight, "raylib [core] example - basic window");
   
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(dir){
            case DERECHA:
                x+=VELOCIDAD;
                if(x>600){
                    dir = ABAJO;
                }
                break;
            case ABAJO:
                y+=VELOCIDAD;
                if(y>600){
                    dir = IZQUIERDA;
                }
                break;
            case IZQUIERDA:
                x-=VELOCIDAD;
                if(x<=0){
                    dir = ARRIBA;
                }
                break;
            case ARRIBA:
                y-=VELOCIDAD;
                if(y<=0){
                    dir = DERECHA;
                }
                break;
                break;
        }
        //----------------------------------------------------------------------------------
 
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(PINK);
 
            DrawText("Hello Ray!", x, y, 40, ORANGE);
 
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------  
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
 
    return 0;
}