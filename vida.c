/*******************************************************************************************
*
*   raylib exercise - life bar
*
*   This exercise has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013 Ramon Santamaria (Ray San)
*
********************************************************************************************/
 
#include "raylib.h"
 
int main()
{
    // Initialization
    //---------------------------------------------------------
    int screenWidth = 600;
    int screenHeight = 220;
   
    InitWindow(screenWidth, screenHeight, "raylib exercise - life bar");
   
    int barWidth = 500;
    int barHeight = 100;
    int barTempWidth = barWidth - 150;
    int barPosX = screenWidth / 2 - barWidth / 2;
    int barPosY = screenHeight / 2 - barHeight / 2;
    int barAutoVariationX = 2;
    int barKeyVariationX = 15;
    Color barFrontColor = GOLD;
    Color barBackColor = MAROON;
    int barBorderThick = 4;
   
    int gameFinish = 0; // (1)You Win, (2)You Loose
   
    const char textWin[40] = "YOU WIN!";
    const char textLoose[40] = "YOU LOOSE...";
   
    const char textGoGo[20] = "Go! Go! Go!";
    const char textOuch[20] = "Ouch!";
    const char textAlmost[20] = "Almost There!";
 
    SetTargetFPS(60);
    //----------------------------------------------------------
   
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //-----------------------------------------------------
        if (gameFinish == 0) barTempWidth -= barAutoVariationX;
       
        if (barTempWidth <= 0)
        {
            barTempWidth = 0;
            gameFinish = 2;
        }
       
        if ((IsKeyPressed(KEY_SPACE)) && (gameFinish == 0)) barTempWidth += barKeyVariationX;
       
        if (barTempWidth >= barWidth)
        {
            barTempWidth = barWidth;
            barFrontColor = LIME;
           
            gameFinish = 1;
        }
        //-----------------------------------------------------
       
        // Draw
        //-----------------------------------------------------
        BeginDrawing();
       
            ClearBackground(RAYWHITE);
           
            DrawRectangle(barPosX - barBorderThick, barPosY - barBorderThick, barWidth + 2 * barBorderThick, barHeight + 2 * barBorderThick, BLACK);
            DrawRectangle(barPosX, barPosY, barWidth, barHeight, barBackColor);
            DrawRectangle(barPosX, barPosY, barTempWidth, barHeight, barFrontColor);
           
            if (gameFinish == 1)
            {
                DrawText(textWin, screenWidth / 2 - MeasureText(textWin, 20) / 2, screenHeight / 2 - 10, 20, BLACK);
            }
            else if (gameFinish == 2)
            {
                DrawText(textLoose, screenWidth / 2 - MeasureText(textLoose, 20) / 2, screenHeight / 2 - 10, 20, BLACK);
            }
           
            if (gameFinish == 0)
            {
                if (barTempWidth <= (barWidth / 3))
                {
                    DrawText(textOuch, screenWidth / 2 - MeasureText(textOuch, 20) / 2, 20, 20, BLACK);
                }
                else if (barTempWidth <= (barWidth - 150))
                {
                    DrawText(textGoGo, screenWidth / 2 - MeasureText(textGoGo, 20) / 2, 20, 20, BLACK);
                }
                else
                {
                    DrawText(textAlmost, screenWidth / 2 - MeasureText(textAlmost, 20) / 2, 20, 20, BLACK);
                }
               
                DrawText("QUICKY! PUSH SPACE!!!", screenWidth / 2 - 110, 175, 20, LIGHTGRAY);  
            }
         
            DrawFPS(10, 10);
       
        EndDrawing();
        //-----------------------------------------------------
    }
 
    // De-Initialization
    //---------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //----------------------------------------------------------
   
    return 0;
}