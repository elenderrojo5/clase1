#include <stdio.h>
 
typedef enum { false, true } bool;
 
bool IsPrime(int num);
 
int main(){
   
    int contador = 0;
    int numero = 0;
   
    while(contador<100){
        if(IsPrime(numero)){
            printf("%d ",numero);
            contador++;
        }
        numero++;
    }
 
    getchar();
   
    return 0;
}
 
bool IsPrime(int num){
   if(num<=3){
       return true;
   }else{
       bool esPrimo = true;
       int actual = num -1;
       while(esPrimo && actual>1){
           if(num%actual == 0){
               esPrimo = false;
           }
           actual--;
       }
       return esPrimo;
   }
}