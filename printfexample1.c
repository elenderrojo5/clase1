#include <stdio.h>

int main()
{
    printf("New line \n");
    getchar();
    printf("Tab \t");
    getchar();
    printf("BackSpace \b");
    getchar();
    printf("carriage return \r");
    getchar();
    
    printf("Integer %i", 2);
    getchar();
    printf("Integer %d", 2);
    getchar();
    printf("Character %c", 'a');
    getchar();
    printf("Float %f", 2.2F);
    getchar();
    printf("String %s", "word");
    getchar();
    return 0;
}