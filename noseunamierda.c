/*******************************************************************************************
*
*   raylib exercise - following eyes
*
*   This exercise has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2013 Ramon Santamaria (Ray San)
*
********************************************************************************************/
 
#include "raylib.h"
#include <math.h>       // Required for sqrt(), pow()
 
void DrawEye(int x, int y, int r, Color col);
 
int main()
{
    // Initialization
    //---------------------------------------------------------
    int screenWidth = 600;
    int screenHeight = 400;
   
    InitWindow(screenWidth, screenHeight, "raylib exercise - following eyes");
   
    int eyeBallsSpace = 200;
    int eyeBallRadio = 80;
   
    int lEyeBallCenterX = screenWidth/2 - eyeBallsSpace/2;
    int lEyeBallCenterY = screenHeight/2;
    int rEyeBallCenterX = screenWidth/2 + eyeBallsSpace/2;
    int rEyeBallCenterY = screenHeight/2;
   
    int lEyePosX = screenWidth/2 - eyeBallsSpace/2;
    int lEyePosY = screenHeight/2;
    int lEyeRadio = 20;
    Color lEyeColor = BROWN;
   
    int rEyePosX = screenWidth/2 + eyeBallsSpace/2;
    int rEyePosY = screenHeight/2;
    int rEyeRadio = 20;
    Color rEyeColor = DARKGREEN;
 
    int mx, my;
    int dx, dy;
    float ax, ay;
    float d;
   
    float rel = 0;
 
    SetTargetFPS(60);
    //----------------------------------------------------------
   
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //-----------------------------------------------------    
        mx = GetMouseX();
        my = GetMouseY();
 
        dx = mx - lEyeBallCenterX;
        dy = my - lEyeBallCenterY;
       
        d = sqrt(pow(dx, 2) + pow(dy,2));
       
        if (d <= (eyeBallRadio - lEyeRadio))    //Inside Eye!
        {
            lEyePosX = mx;
            lEyePosY = my;
        }
        else    //Outside Eye!
        {
            rel = (eyeBallRadio - lEyeRadio) / d ;
           
            ax = rel * dx;
            ay = rel * dy;
           
            lEyePosX = lEyeBallCenterX + (int)ax;
            lEyePosY = lEyeBallCenterY + (int)ay;
        }
       
        dx = mx - rEyeBallCenterX;
        dy = my - rEyeBallCenterY;
       
        d = sqrt(pow(dx, 2) + pow(dy,2));
       
        if (d <= (eyeBallRadio - rEyeRadio))    //Inside Eye!
        {
            rEyePosX = mx;
            rEyePosY = my;
        }
        else    //Outside Eye!
        {
            rel = (eyeBallRadio - rEyeRadio) / d ;
           
            ax = rel * dx;
            ay = rel * dy;
           
            rEyePosX = rEyeBallCenterX + (int)ax;
            rEyePosY = rEyeBallCenterY + (int)ay;
        }
       
        //-----------------------------------------------------
       
        // Draw
        //-----------------------------------------------------
        BeginDrawing();
       
            ClearBackground(RAYWHITE);
           
            DrawCircle(lEyeBallCenterX, lEyeBallCenterY, eyeBallRadio, LIGHTGRAY);
            DrawCircle(rEyeBallCenterX, rEyeBallCenterY, eyeBallRadio, LIGHTGRAY);
               
            DrawEye(lEyePosX, lEyePosY, lEyeRadio, lEyeColor);
            DrawEye(rEyePosX, rEyePosY, rEyeRadio, rEyeColor);
           
            //DrawLine(lEyeBallCenterX, lEyeBallCenterY, mx, my, MAROON);
            //DrawLine(rEyeBallCenterX, rEyeBallCenterY, mx, my, MAROON);      
 
            DrawFPS(10, 10);
       
        EndDrawing();
        //-----------------------------------------------------
    }
 
    // De-Initialization
    //---------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //----------------------------------------------------------
   
    return 0;
}
 
void DrawEye(int x, int y, int r, Color col)
{
    DrawCircle(x, y, r, col);
    DrawCircle(x, y, r/2, BLACK);
}