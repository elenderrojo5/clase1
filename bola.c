/*******************************************************************************************
*
*   raylib [core] example - Keyboard input
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (@raysan5)
*
********************************************************************************************/
 
#include "raylib.h"
 
//typedef vector2_t{
//    int x;
//    int y;
//}Vector2;
 
#define VELOCIDAD 20.0f
#define RADIO 5
 
int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 800;
    int framesCounter = 0;
    int x = 0;
    int y = 0;
    InitWindow(screenWidth, screenHeight, "raylib [core] example - keyboard input");
 
    Vector2 ballPosition = { (float)screenWidth/2, (float)screenHeight/2 };
 
    SetTargetFPS(60);       // Set target frames-per-second
    //--------------------------------------------------------------------------------------
 
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        if (IsKeyDown(KEY_RIGHT)) ballPosition.x += VELOCIDAD;
        if (IsKeyDown(KEY_LEFT)) ballPosition.x -= VELOCIDAD;
        if (IsKeyDown(KEY_UP)) ballPosition.y -= VELOCIDAD;
        if (IsKeyDown(KEY_DOWN)) ballPosition.y += VELOCIDAD;
        //----------------------------------------------------------------------------------
        
        framesCounter++;
        
        if (((framesCounter/10)%2) == 1){
            
            ballPosition.x+=VELOCIDAD;
            framesCounter = 0;
            
        }
        
        if(ballPosition.x<RADIO){
            ballPosition.x = RADIO;
        }else if(ballPosition.x>screenWidth-RADIO){
            ballPosition.x = screenWidth-RADIO;
        }
       
        if(ballPosition.y<RADIO){
            ballPosition.y = RADIO;
        }else if(ballPosition.y>screenHeight-RADIO){
            ballPosition.y = screenHeight-RADIO;
        }
       
       x = ballPosition.x;
       y = ballPosition.y;
       
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            ClearBackground(RAYWHITE);
 
            DrawText("move the ball with arrow keys", 10, 10, 20, DARKGRAY);
 
            DrawCircleV(ballPosition, RADIO, MAROON);
            
            DrawText(FormatText("posicion x %i", x), 0, 0, 20, RED);
            DrawText(FormatText("posicion y %i", y), 0, 25, 20, RED);
 
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
 
    return 0;
}