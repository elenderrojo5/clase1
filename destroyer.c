#include <raylib.h>
 
#define VELOCIDAD 4
#define PLAYERWIDTH 100
#define PLAYERHEIGHT 20
#define BLOCK_WIDTH PLAYERWIDTH
#define BLOCK_HEIGTH PLAYERHEIGHT*2
 
typedef struct block_t{
    Rectangle rec;
    Color col;
    bool alive;
}block;
 
block initBlock(int columna, int fila);
void drawBlock(block bloque);
 
int main(){
   
   
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 1000;
    int screenHeight = 768;
   
    InitWindow(screenWidth, screenHeight, "Breakout");
   
    SetTargetFPS(60);
   
    Vector2 ball;
    ball.x = screenWidth/2;
    ball.y = screenHeight/2;
   
    int ballRadious = 20;
   
    Vector2 ballAcceleration;
    ballAcceleration.x = VELOCIDAD;
    ballAcceleration.y = VELOCIDAD;
   
    Rectangle player = { screenWidth/2 - PLAYERWIDTH/2, screenHeight - (PLAYERHEIGHT + 20), PLAYERWIDTH, PLAYERHEIGHT };
   
   
    block bloques[10][10];
   
    for(int i=0;i<10;i++){
        for(int j=0;j<10;j++){
            bloques[i][j] = initBlock(i,j);
        }
    }
   
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        //Update
       
        //Movemos el player
        if (IsKeyDown(KEY_LEFT)){
            player.x-=VELOCIDAD;
        }
        if (IsKeyDown(KEY_RIGHT)){
            player.x+=VELOCIDAD;
        }
       
        if(player.x<0){
            player.x = 0;
        }
       
        if(player.x > screenWidth -PLAYERWIDTH ){
            player.x = screenWidth -PLAYERWIDTH;
        }
       
        ball.x += ballAcceleration.x;
        ball.y += ballAcceleration.y;
       
        if(ball.y<ballRadious){
            ballAcceleration.y *= -1;
        }
       
        if(ball.x<ballRadious){
            ballAcceleration.x *= -1;
        }
       
        if(ball.y>screenHeight-ballRadious){
            ballAcceleration.y *= -1;
        }
       
        if(ball.x>screenWidth-ballRadious){
            ballAcceleration.x *= -1;
        }
       
        // Check Collision with Player
        if(CheckCollisionCircleRec(ball,ballRadious,player)){
            ballAcceleration.y = -VELOCIDAD;
        }
       
        // Check Collision with any block
        for(int i=0;i<10;i++){
            for(int j=0;j<10;j++){
                if(bloques[i][j].alive && CheckCollisionCircleRec(ball,ballRadious,bloques[i][j].rec)){
                    //ballAcceleration.y = VELOCIDAD;
                    bloques[i][j].alive = false;
                }
            }
        }
       
       
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
 
            // Limpio pantalla
            ClearBackground(RAYWHITE);
 
            // Pintamos Pelota
            DrawCircleV(ball,ballRadious,RED);
           
            //Pintamos Pala
            DrawRectangleRec(player,BROWN);
           
            //Pintamos el bloque
            for(int i=0;i<10;i++){
                for(int j=0;j<10;j++){
                    drawBlock(bloques[i][j]);
                }
            }
           
           
        EndDrawing();
        //----------------------------------------------------------------------------------
    }
 
    // De-Initialization
    //--------------------------------------------------------------------------------------
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
     
   
    return 0;
}
 
 
block initBlock(int columna, int fila){
    block bloque_salida;
    bloque_salida.alive = true;
    bloque_salida.col.r = GetRandomValue(0, 255);
    bloque_salida.col.g = GetRandomValue(0, 255);
    bloque_salida.col.b = GetRandomValue(0, 255);
    bloque_salida.col.a = 255;
    bloque_salida.rec.x = BLOCK_WIDTH*columna;
    bloque_salida.rec.y = BLOCK_HEIGTH*fila;
    bloque_salida.rec.width = BLOCK_WIDTH;
    bloque_salida.rec.height = BLOCK_HEIGTH;    
   
    return bloque_salida;
}
 
void drawBlock(block bloque){
    if(bloque.alive){
        DrawRectangleRec(bloque.rec,bloque.col);
    }
}